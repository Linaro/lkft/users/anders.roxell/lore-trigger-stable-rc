#!/bin/bash
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

KERNEL_URL=${KERNEL_URL:-"https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git"}
LIST_PATH=${LIST_PATH:-"mainlinglists/lore.kernel.org/stable"}

for ml_path in $(ls -1v ${LIST_PATH}); do
	git -C ${LIST_PATH}/${ml_path} log --oneline | grep '\w \[PATCH .* review$'|tee -a kernel-rc-reviews
done


for release in $(cat kernel-rc-reviews| awk -F ' ' '{print $3}'); do
	echo ${release}
	git ls-remote ${KERNEL_URL} linux-${release}.y | tee kernel-ls-remote
	branch=$(cat kernel-ls-remote | awk -F '/' '{print $NF}')
	sha=$(cat kernel-ls-remote | awk -F ' ' '{print $1}')

	echo "Kernel branch: ${branch}"
	echo "Kernel sha: ${sha}"
	echo "CI_JOB_ID: ${CI_JOB_ID}"

	./trigger-pipeline.py \
		--trigger-project-id ${TRIGGER_PROJECT_ID} \
		--triggered-from ${CI_JOB_ID} \
		--kernel-tree ${KERNEL_URL} \
		--kernel-branch ${branch} \
		--kernel-sha ${sha}
done
