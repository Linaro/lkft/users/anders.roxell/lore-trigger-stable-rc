#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse
import logging
import re
import requests
import subprocess as sp
from pathlib import Path
from urllib.parse import urlparse
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def arg_parser():
    parser = argparse.ArgumentParser(description="Get lore repositories")

    parser.add_argument(
        "--lore-url",
        required=False,
        default="http://lore.kernel.org/stable",
        help="Url to lore mailing list",
    )

    parser.add_argument(
        "--all-repositories",
        action="store_true",
        default=False,
        help="Date for the shallow clone",
    )

    parser.add_argument(
        "--shallow-date",
        required=False,
        default=None,
        help="Date for the shallow clone",
    )

    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Display debug messages",
    )

    return parser


def get_tree_base_path(tree):
    url = urlparse(tree)

    tree_base_path = os.path.dirname(f"{url.netloc}/{url.path}")
    tree_name = os.path.basename(url.path).replace(".git", "")
    tree_base_path = Path("mailinglists") / tree_base_path
    tree_base_path.mkdir(parents=True, exist_ok=True)
    tree_base_path /= tree_name

    return tree_base_path


def run_git(*args):
    cmd = ["git"] + list(args)
    logger.info(f"Running {cmd}")
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    stdout, stderr = proc.communicate()
    proc.ok = proc.returncode == 0

    proc.out = stdout.decode()
    proc.err = stderr.decode()
    return proc


def git_pull(tree, tree_base_path):
    return run_git("clone", tree, tree_base_path, "pull")


def git_clone(tree, tree_base_path):
    return run_git("clone", tree, tree_base_path)


def git_shallow_clone(tree, tree_base_path, date):
    shallow_since = f"--shallow-since={date}"
    return run_git("clone", tree, tree_base_path, shallow_since)


def scan_for_repo(url, all_repos=False):
    full_url = f"{url}/_/text/mirror/"
    logger.debug(f"{full_url=}")
    resp = requests.get(full_url)
    repos = ""
    if resp.status_code >= 200 and resp.status_code < 300:
        for line in resp.text.split('\n'):
            if re.match(f".*?{url}/\\d+.*", str(line)):
                # isolate the url and set it to be last one seen
                fields = line.split('"')
                logger.debug(f"{fields=}")
                for x in fields:
                    if x.startswith('http'):
                        if all_repos:
                            repos = f"{repos} {x}"
                        else:
                            repos = x

    return repos


if __name__ == "__main__":
    args = arg_parser().parse_args()
    if args.debug:
        logger.setLevel(level=logging.DEBUG)
    logger.debug(f"{args.lore_url=}")

    url = scan_for_repo(args.lore_url, all_repos=args.all_repositories)

    for tree in url.strip().split(" "):
        tree_base_path = get_tree_base_path(tree)
        if tree_base_path.exists():
            git_pull(tree, tree_base_path)
        elif args.shallow_date:
            git_shallow_clone(tree, tree_base_path, args.shallow_date)
        else:
            git_clone(tree, tree_base_path)
