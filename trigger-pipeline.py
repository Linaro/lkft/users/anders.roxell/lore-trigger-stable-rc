#!/usr/bin/env python3
# vim: set ts=8 sw=4 sts=4 et tw=80 fileencoding=utf-8 :
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT
import argparse
import json
import os
import requests
import sys

def arg_parser():
    parser = argparse.ArgumentParser(description="Get lore repositories")

    parser.add_argument(
        "--trigger-project-id",
        required=True,
        help="The ID of the project to be triggered",
    )

    parser.add_argument(
        "--triggered-from",
        required=False,
        default="",
        help="Describe from where the trigger happened",
    )

    parser.add_argument(
        "--trigger-project-branch",
        required=False,
        default="main",
        help="The branch of the project to be triggered",
    )

    parser.add_argument(
        "--kernel-tree",
        required=False,
        default="https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
        help="Kernel tree url",
    )

    parser.add_argument(
        "--kernel-branch",
        required=True,
        help="Kernel branch",
    )

    parser.add_argument(
        "--kernel-sha",
        required=True,
        help="Kernel sha",
    )

    return parser


if __name__ == "__main__":
    args = arg_parser().parse_args()

    gitlab_url = os.environ.get("GITLAB_SERVER_URL", "https://gitlab.com")
    private_token = os.environ.get("GITLAB_PRIVATE_TOKEN") or sys.exit(
        "GITLAB_PRIVATE_TOKEN not set"
    )
    variables = {
        "TRIGGERED_FROM": args.triggered_from,
        "KERNEL_URL": args.kernel_tree,
        "KERNEL_BRANCH": args.kernel_branch,
        "KERNEL_SHA": args.kernel_sha,
}
    data = {
        "token": private_token,
        "ref": args.trigger_project_branch,
        "variables": variables,
    }
    url = f"{gitlab_url}/api/v4/projects/{args.trigger_project_id}/trigger/pipeline"
    response = requests.post(url, json=data)
    for k,v in json.loads(response.text).items():
        if k == 'web_url':
            print(f"Triggered pipeline: {v}")
